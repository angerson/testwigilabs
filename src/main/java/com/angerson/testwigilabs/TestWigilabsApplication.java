package com.angerson.testwigilabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestWigilabsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestWigilabsApplication.class, args);
    }

}
